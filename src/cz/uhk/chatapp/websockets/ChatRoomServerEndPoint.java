package cz.uhk.chatapp.websockets;

import java.io.IOException;
import java.io.StringWriter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/ChatRoomServerEndPoint")
public class ChatRoomServerEndPoint {
	//uchov�v� p�ipojen� u�ivatele
	static Set<Session> chatRoomUsers = Collections.synchronizedSet(new HashSet<Session>());
	
	//nav�z�n� nov�ho spojen�
	@OnOpen
	public void handleOpen(Session userSession){
		chatRoomUsers.add(userSession);		
	}
	
	//ukon�en� spojen�
	@OnClose
	public void handleClose(Session userSession){
		chatRoomUsers.remove(userSession);		
	}
	@OnMessage
	public void handleMessage(String message, Session userSession){
		String userName = (String) userSession.getUserProperties().get("username");
		//prvn� spr�va je pro nastaven� jm�na
		if(userName == null){
			userSession.getUserProperties().put("username", message);
			try {
				userSession.getBasicRemote().sendText(buildJsonData("System", "jste p�ipojen� pod jm�nem: " + message));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 
		else { //pak u� se jen pos�laj� spr�vy ostatn�m
			Iterator<Session> iterator = chatRoomUsers.iterator();
			while(iterator.hasNext()){
				try {
					iterator.next().getBasicRemote().sendText(buildJsonData(userName, message));
				} catch (IOException e) {
					e.printStackTrace();
				}				
			}
		}
	}
	private String buildJsonData(String username, String message) {
		JsonObject jonObject = Json.createObjectBuilder().add("message", username + ": " + message).build();
		StringWriter stringWriter = new StringWriter();
		JsonWriter jsonWriter = Json.createWriter(stringWriter);
		jsonWriter.write(jonObject);
		return stringWriter.toString();
	}
	

}
